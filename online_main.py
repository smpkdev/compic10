# How to compile:
# write source_code in between the """
# press "Run" (set optional flags)
# click the button in the bottom console to copy result to clipboard
# Language Reference: https://gitlab.com/smpkdev/compic10/-/blob/b2f742e994526a8aa31f3db6240870c8293ef465/README.md

source_code = """
# Program lets AIMEe move in squares
# groundplane is x,z-plane

# procedure with parameters x, z, d where d has the default value 10 if not used in call 
proc move_to(x, z, d=10)
	# access device attached to IC10 (in this case AIMEe) which is always keyword 'self' you would do this through device 'db' in IC10-MIPS
	self.TargetX = x
	self.TargetZ = z
	self.Mode=2
	while(true)
		if(((self.PositionX-x)*(self.PositionX-x)+(self.PositionZ-z)*(self.PositionZ-z))<d)
			break
		end
	end
	self.Mode=0
end

main
	# safe start coordinates
	var base_x = self.PositionX
	var base_z = self.PositionZ
	
	while(true)
		# function calls (defaults parameters get set automatically)
		move_to(base_x+50, base_z+50)
		move_to(base_x+50, base_z-50)
		move_to(base_x-50, base_z-50)

		# without default parameter
		move_to(base_x-50, base_z+50, 5)
	end
end
"""
optimize_code = True  # set False if you want the flag --no_optimization
annotate_code = False # set True if you want the flag --annotate






























################################################################################
#################### DONT CHANGE ANYTHING FROM HERE ON #########################
################################################################################


from tokens import resolve_tokens
from syntax import syntactic_analysis
from codegen import create_asm_code
from compilerexception import CompilerException, UnexpectedException
from programdatabase import ProgramDatabase

if __name__=="__main__":
    try:
        ProgramDatabase.VAR_STACK_LENGTH = 100
        ProgramDatabase.VAR_STACK_OFFSET = 400
        ProgramDatabase.ANNOTATE = annotate_code
        ProgramDatabase.CODE_OPTIMIZATION = optimize_code

        if ProgramDatabase.VAR_STACK_OFFSET+ProgramDatabase.VAR_STACK_LENGTH>=512:
            raise CompilerException("Stacklength not high enough to support varstackoffset and varstacklength configuration.")

        program = source_code.split("\n")
        code = []  # Format: string, True:raw code False:syntax for code generation
        for i, line in enumerate(program):
            ProgramDatabase.set_compiler_position(i)
            tokens = resolve_tokens(line)
            syntax = syntactic_analysis(tokens)
            code.append(syntax)
        pd = ProgramDatabase(len(code))
        asm_code = create_asm_code(code, ProgramDatabase.BType.Global_, database=pd)[:-1]  # ommit final newline
        if not pd.has_main():
            raise CompilerException("No main block.")
        if ProgramDatabase.CODE_OPTIMIZATION:
            asm_code = asm_code.replace("move ra sp\nmove sp ra\n", "")  # can't always handled within a single expression
            asm_code = asm_code.replace("move sp ra\nmove ra sp\n", "")  # can't always handled within a single expression
            asm_code = asm_code.replace("j main\nmain:\n", "")
        print(asm_code)
    except CompilerException as ce:
        print(str(ce))
    except UnexpectedException as ue:
        print(str(ue))
